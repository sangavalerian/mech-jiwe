import serial
import time
from matplotlib import pyplot as plt

class MechJiwe(object):

    def __init__(self,port):
        self.ser = serial.Serial(port)

    def clear_buffer(self):
        self.ser.read(self.ser.inWaiting())

    def move(self,steps):
        assert type(steps) is int, 'Steps must be an interger'
        if steps==0:
            pass
        else:
            self.ser.write('move %d\r'%steps)
        tmp = self.ser.readline()
    
    def set_speed(self,RPM):
        assert type(RPM) is int, 'Speed (RPM) must be an interger'
        assert RPM >0, 'Speed (RPM) must be larger than 0'
        self.ser.write('set_speed %d\r'%RPM)
        tmp = self.ser.readline()

    def load(self,raw=False):
        self.clear_buffer()
        if raw:
            self.ser.write('load \r')
        else:
            self.ser.write('load \r')
        F = float(self.ser.readline())
        return F

    def tare(self):
        self.clear_buffer()
        self.ser.write('tare\r')
        tmp = self.ser.readline()
    
    def move_show(self,num,step):
      p=0
      ps=[]
      Fs=[]
      for n in range(num):
          self.move(step)
          p+=step
          ps.append(p/200.0)
          Fs.append(self.load()/1000.0)
      plt.plot(ps,Fs)
      plt.xlabel('Distance (mm)')
      plt.ylabel('Force (N)')
      plt.show()
      return (ps,Fs)
