## Mech-Jiwe will be a mechanical tester for forces of order a few newtons

The idea is to build a mechanical tester which uses a welded frame, a number of 3D-printed parts, an arduino, and a few other simple components. We hope to use the tester to test the mechanical properties of 3D printer filament and 3D printed parts.

## Status: Very incomplete

Build notes are available in the build notes directory and give an idea of the development as it happens. Many parts of the MechJiwe are from the RETR3D open source printer. A list of these files is in the build notes.

## TODO:
1. Move all design files to an open standard
1. Find a working link to the RETR3D printer files for the interim period
1. Fix arduino code so motors used are mirrored can be used mirrored in hardware
1. Fix documentation so somone else can actually use this project