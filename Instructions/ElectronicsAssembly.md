# Electronics for the Mech-Jiwe

## Parts Needed

* 1x Micro load cell
* 1x Sparkfun Load Cell Amp 
* 1x Arduino CNC shield v3
* 2x A4988 Stepper Motor Driver Carrier with heat sink
* 2x 2-pin Short Circuit Jumper Caps
* 1x Arduino Due
* 2x Nema17 stepper motors with 2.54 mm pitch connectors
* 5x Female-Female jumper cables
* 1x 4 or 5 socket female jumper connector, cimp or solder
* 1x 12v (3A) power supply compatible with Due
* 1x micro-USB cable (data and power!)
* Some reasonably thick gauge wire (red and black)
* Assorted heatshrink

## Tools Needed

* Wire cutters
* Wire strippers
* Soldering iron
* Multimeter
* Heat gun
* Crimp tool- only if crimping own connectors

## Steps

1. Attach CNC shield to Arduino Due
    1. Align the pin labelled **RST** on the CNC shield with the connector labelled **RST** on the Arduino
    1. Push shield onto Arduino
    ![](./ims/01-Sheild.jpg) 
1. Making power supply
    1. Cut power connector off power supply with about 10cm from the connector.
    1. Separate the first 3 cm of the positive and negative wires of the power supply for both the plug and connector cables.
    1. Use multimeter to check which cable is positive.
    1. Strip about 5 mm of clean wire on both plug and connector sides of these cables.
    1. Get about 10 cm of red wire and 10 cm of black wire. Also strip both ends.
    1. Use a soldering iron to tin all 6 bare cables
    1. Feed heatshrink over both the red wire, and the positive cable of the connector cable
    1. Feed heatshrink over the black wire and the negative cable of the connector cable
    1. Feed heatshrink over both wires of the plug cable
    1. Solder the red wire, and both positive wires of the cables into one joint
    1. Solder the black wire, and both negative wires of the cables into one joint
    1. Pull the heatshrink from the black wire and from the red wire over the two solder joints and shrink with heat gun
    1. Pull the other peice of heatshrink over the  where the cables split and shrink with heat gun
    1. Connect the red and black wires into the power input block on the CNC shield
     ![](./ims/00-Wire.jpg) 
    1. Connect the power supply into the socket on the Due
    1. **Do not plug in the power supply yet**
    ![](./ims/02-PowerArduino.jpg) 
1. Connect up CNC shield drivers
    1. Hold CNC shield with RST button in top left corner
    1. Insert A4988 into the connector labelled X (top left motor connector) so that the silver adjusting pot is at the bottom
    1. Insert A4988 into the connector labelled A (bottom right motor connector) so that the silver adjusting pot is at the bottom
    1. Locate the 4 rows of 4 pins on the left side of the board labelled X, Y, Z, D12.
    1. Place short circuit jumper cap on both pairs of pins in the row labelled X
    ![](./ims/03-Jumper_n_A4988.jpg)
1. Connect motors to CNC shield
    1. Check the motor for a part number
    1. Locate the data sheet for the motor (Google it!?)
    1. In the data sheet find which colours refer to the connections A+,A-,B+,B-
    1. Connect one motor to the connector to the left of the X A4988 driver. The pin order from top to bottom is B+,B-,A+,A-
    1. Connect one motor to the connector to the left of the A A4988 driver. The pin order from top to bottom is B+,B-,A+,A-
    ![](./ims/04-Motors.jpg)
1. Connect up the load cell and amplifier
    1. Check the micro load cell has 4 wires which are Red, Black, White and Green.
    1. Solder or crimp these onto the female jumper connector in the order Red, Black, White, Green.
    1. If heatshrink the connector if you want to
    1. This connector should fit onto the one side of the Sparkfun Load Cell Amp. The pins are labelled with the colours
    1. Use a jumper cable to connect VDD on the Load Cell Amp to the 3V3 pin on the CNC shield (one of the 8 pins in the far corner from the power block.
    1. Use a jumper cable to connect VCC on the Load Cell Amp to the 5V pin on the CNC shield (by the 3V3 pin)
    1. Use a jumper cable to connect GND on the Load Cell Amp to the GND pin on the CNC shield (by the 5V pin)
    1. Use a jumper cable to connect DAT on the Load Cell Amp to the pin 20 on the Arduino Due
    1. Use a jumper cable to connect CLK on the Load Cell Amp to the pin 21 on the Arduino Due
    ![](./ims/05-LoadCellAmp.jpg)
    ![](./ims/06-LoadCellConnection.jpg)
    ![](./ims/07-All.jpg)
1.  Load the firmware.
    1. Plug in the power supply.
    1. Plug in the micro-USB to the programming port on the DUE (the micro-USB port closer to the power connector)
    1. Plug the other end of the USB cable into a computer
    1. Open the Arduino IDE (tested with V1.8.5) and load in the Mech-Jiwe firmware from the src folder
    1. Set the Arduino IDE board to DUE, and set the port in the Tools menu
    1. Upload using the arrow icon in the top right.
    1. Once upload is complete you should be ready to run the python code!
