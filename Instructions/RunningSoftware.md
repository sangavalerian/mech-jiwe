# Running the software

Currently the software is simply a python library with minimal control

1. Find which port the MechJiwe connected on using the arduino IDE, for example `/dev/ttyACM0` or `COM1`
1. To run open a python terminal in the `lib` directory.
1. Run the following command to import the library:
    `from MechJiwe import MechJiwe as MJ`
1. Run the following command **replacing YOURPORT with the port given by the arduino IDE**:
    `mj = MJ('YOURPORT')`
1. This should connect! Now you can run the following commands:
    1. `mj.tare()` - sets the load cell to read zero
    1. `mj.load()` - gives the load on the load cell in mN
    1. `mj.move(n_steps)` - **replace n_steps with an integer number** this will move n steps. Positive number moves up, negative moves down
1. Once the balance is in place and a piece is connected into the tester run the following command to take a measurement:
    `mj.move_show(num,n_steps)` - **replace num and n_steps with integer numbers** The balance will move n_steps and then measure the load. It will repeat this num times and plot a graph
1. You can save the graph with the save icon. The raw data is returned to the screen once the graph is closed

This software needs to be made significantly better!!